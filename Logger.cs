﻿using System;
using System.Configuration;
using System.IO;

namespace PrintArchive2
{
    class Logger
    {

        public static void LogMess(string message)
        {
            string logDir = ConfigurationManager.AppSettings["LogDir"];
            string logFileName = "fileMoverLog.txt";

            DateTime dtNow = DateTime.Now;

            if (Directory.Exists(logDir))
            {
                File.AppendAllText(Path.Combine(logDir, logFileName), string.Format("{0} -> {1}", dtNow, message) + Environment.NewLine);
            }
            else
            {
                Directory.CreateDirectory(logDir);
            }

        }

    }
}
