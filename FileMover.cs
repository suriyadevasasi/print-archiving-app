﻿using System;
using System.Configuration;
using System.IO;

namespace PrintArchive2
{
    class FileMover
    {
        public static void syncAllFilesFirstTime()
        {
            Logger.LogMess("Starting ......");
            string strDesPath = ConfigurationManager.AppSettings["DestinationSource"];
            string strSourcePath = ConfigurationManager.AppSettings["PrimarySource"];
            string destSubDirectory = strDesPath + DateTime.Now.ToString("yyyy-MM-dd");

            if (!Directory.Exists(destSubDirectory))
            {

                Directory.CreateDirectory(destSubDirectory);

            }


            //Get list of files from sourcepath
            string[] arrFiles = System.IO.Directory.GetFiles(strSourcePath);
            Logger.LogMess(string.Format("File count: {0}", arrFiles.Length));

            foreach (string sourceFiles in arrFiles)
            {
                //get filename
                string strFileName = System.IO.Path.GetFileName(sourceFiles);
                string strDesFilePath = string.Format(@"{0}\{1}", destSubDirectory, strFileName);
                //check whether the destination path contatins the same file
                if (!File.Exists(strDesFilePath))
                {

                    File.Move(sourceFiles, strDesFilePath);
                    Logger.LogMess(string.Format("Moving file from: {0} to {1}", sourceFiles, strDesFilePath));

                }
            }

        }


        public static void syncUpdatedFiles(string strSourceFile)
        {
            string strDesPath = ConfigurationManager.AppSettings["DestinationSource"];
            string destSubDirectory = strDesPath + DateTime.Now.ToString("yyyy-MM-dd");
            string strFileName = Path.GetFileName(strSourceFile);
            string strDesFilePath = string.Format(@"{0}\{1}", destSubDirectory, strFileName);
            var val = File.Exists(strDesFilePath);
            //check whether the destination path contatins the same file
            if (!File.Exists(strDesFilePath))
            {
                for (; ; )
                {
                    if (IsFileLocked(strSourceFile))
                    {
                        File.Move(strSourceFile, strDesFilePath);
                        break;
                    }
                }
            }


            if (File.Exists(strSourceFile))
            {
                Logger.LogMess("The original file still exists, which is unexpected.");
            }
            else
            {
                Logger.LogMess("The original file no longer exists, which is expected.");
            }

        }


        public static void Watch()
        {
            string strSourcePath = ConfigurationManager.AppSettings["PrimarySource"];

            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = strSourcePath;
            //watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = "*.*";
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.EnableRaisingEvents = true;
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            Logger.LogMess("File: " + e.FullPath + " " + e.ChangeType);
            syncUpdatedFiles(e.FullPath);
        }

        private static bool IsFileLocked(string strSourceFile)
        {
            string strSourcePath = ConfigurationManager.AppSettings["PrimarySource"];

            try
            {
                using (Stream stream = new FileStream(strSourcePath, FileMode.Open))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

    }
}
